# react-collections
[![License: GPL v3](https://img.shields.io/badge/License-GPL_v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

Is a loosely related rant of react parts that I found useful and don't want to search all 
over tha place. 

The need is there because if you did someething useful and did not upload it immediately its gone. 

That is just how the scheme works.

Every saved component is a victory. And every threat is a confirmation.


- [react-collections](#react-collections)
  - [Why](#why)

## Why 

Save time. Make the most out of courses and clients. 